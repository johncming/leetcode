package leetcode

import (
	"sort"
)

func threeSumClosest(nums []int, target int) int {
	res := nums[0] + nums[1] + nums[len(nums)-1]
	sort.Ints(nums)

	for i := 0; i < len(nums)-2; i++ {
		start, end := i+1, len(nums)-1
		for start < end {
			tmp := nums[i] + nums[start] + nums[end]
			if tmp > target {
				end--
			} else {
				start++
			}
			if abs(tmp, target) < abs(res, target) {
				res = tmp
			}
		}
	}

	return res
}

func abs(a, b int) int {
	if a > b {
		return a - b
	} else {
		return b - a
	}
}
