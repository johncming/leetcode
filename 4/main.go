package leetcode

// 1 3  5 || 7 9    cut1=3
// 2 4  || 6  8 10  cut2=2
// 1 2 3 4 5 || 6 7 8 9 10  cut2 = (size/2) - cut1
// size = 10 5-1 =4
// size =9 4-1 = 3 奇数左边数量比右边少一
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {

	if len(nums1) > len(nums2) {
		return findMedianSortedArrays(nums2, nums1)
	}

	if len(nums1) == 0 {
		return float64(nums2[(len(nums2)-1)/2]+nums2[len(nums2)/2]) / 2
	}

	var cut1, cut2 int
	cutL, cutR := 0, len(nums1)
	size := len(nums1) + len(nums2)

	var L1, L2, R1, R2 int
	min := -1 << 63
	max := 1<<63 - 1

	for cut1 <= len(nums1) {
		cut1 = (cutR-cutL)/2 + cutL
		cut2 = size/2 - cut1

		switch cut1 {
		case 0:
			L1 = min
		default:
			L1 = nums1[cut1-1]
		}
		switch cut2 {
		case 0:
			L2 = min
		default:
			L2 = nums2[cut2-1]
		}
		switch cut1 {
		case len(nums1):
			R1 = max
		default:
			R1 = nums1[cut1]
		}
		switch cut2 {
		case len(nums2):
			R2 = max
		default:
			R2 = nums2[cut2]
		}

		switch {
		case L1 > R2:
			cutR = cut1 - 1
		case L2 > R1:
			cutL = cut1 + 1
		default:
			if size%2 == 0 {
				if L2 > L1 {
					L1 = L2
				}
				if R2 < R1 {
					R1 = R2
				}
				return float64(L1+R1) / 2
			} else {
				if R2 < R1 {
					R1 = R2
				}
				return float64(R1)
			}
		}

	}

	return -1
}
