package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	nums1 := []int{1, 3}
	nums2 := []int{2}

	res := findMedianSortedArrays(nums1, nums2)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	// 1 2 3 4
	nums1 := []int{1, 2}
	nums2 := []int{3, 4}

	res := findMedianSortedArrays(nums1, nums2)
	fmt.Printf("%+v\n", res) // output for debug
}
