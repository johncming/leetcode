package leetcode

func lengthOfLongestSubstring(s string) int {
	var res int

	if len(s) == 0 {
		return res
	}

	buf := make(map[byte]int)

	start, end := 0, 0
	for end < len(s) {
		if v, ok := buf[s[end]]; ok {
			start = max(start, v+1)
		}
		buf[s[end]] = end
		// 区别于set方法，map方法是计算下标的
		res = max(res, end-start+1)
		end++
	}

	return res
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
