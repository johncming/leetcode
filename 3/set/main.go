package leetcode

func lengthOfLongestSubstring(s string) int {
	var res int

	if len(s) == 0 {
		return res
	}

	// set, 这里value是没有用的
	buf := make(map[byte]struct{})

	start := 0 // 窗口开始
	end := 0   // 窗口结束
	for end < len(s) {
		if _, ok := buf[s[end]]; ok {
			delete(buf, s[start])
			start++
		} else {
			buf[s[end]] = struct{}{}
			end++
			// 添加字符时候，计算最长
			res = max(res, len(buf))
		}
	}

	return res
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
