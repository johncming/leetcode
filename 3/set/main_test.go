package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	s := "abcabcbb"
	res := lengthOfLongestSubstring(s)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	s := "aab"
	res := lengthOfLongestSubstring(s)
	fmt.Printf("%+v\n", res) // output for debug
}
