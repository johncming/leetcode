package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	s := "PAYPALISHIRING"
	numRows := 3
	res := convert(s, numRows)
	fmt.Printf("%+v\n", res) // output for debug
}
