package leetcode

func convert(s string, numRows int) string {
	if numRows <= 1 {
		return s
	}

	buf := make([][]byte, numRows)

	for i := 0; i < len(s); i++ {
		index := i % (2*numRows - 2)
		if index >= numRows {
			index = 2*numRows - 2 - index
		}
		buf[index] = append(buf[index], s[i])
	}

	var res string
	for i := 0; i < len(buf); i++ {
		res += string(buf[i])
	}

	return res
}
