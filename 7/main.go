package leetcode

func reverse(x int) int {
	min := -1 << 31
	max := 1<<31 - 1

	var res int
	for x != 0 {
		res = x%10 + res*10
		x /= 10
		if res < min || res > max {
			return 0
		}
	}
	return res
}
