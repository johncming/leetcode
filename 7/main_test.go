package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	x := 123
	res := reverse(x)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	x := -123
	res := reverse(x)
	fmt.Printf("%+v\n", res) // output for debug
}
