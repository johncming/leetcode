package leetcode

func twoSum(nums []int, target int) []int {
	ret := []int{-1, -1}
	if len(nums) == 0 {
		return ret
	}

	buf := make(map[int]int)

	for i := 0; i < len(nums); i++ {
		if v, ok := buf[target-nums[i]]; ok {
			ret[0] = v
			ret[1] = i
			break
		}
		// key是值，value对应index
		buf[nums[i]] = i
	}

	return ret
}
