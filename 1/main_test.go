package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	nums := []int{2, 7, 11, 15}
	target := 9
	res := twoSum(nums, target)
	fmt.Printf("%+v\n", res) // output for debug
}
