package leetcode

func longestPalindrome(s string) string {
	var res string

	if len(s) == 0 {
		return res
	}

	for i := 0; i < len(s); i++ {
		helper(&res, s, i, i)
		helper(&res, s, i, i+1)
	}

	return res
}

// b a b a d
//     b
func helper(res *string, s string, left, right int) {
	for left >= 0 && right < len(s) && s[left] == s[right] {
		left--
		right++
	}
	cur := s[left+1 : right]
	if len(cur) > len(*res) {
		*res = cur
	}
}
