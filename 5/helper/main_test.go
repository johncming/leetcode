package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	s := "babad"
	res := longestPalindrome(s)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	s := "abcda"
	res := longestPalindrome(s)
	fmt.Printf("%+v\n", res) // output for debug
}
