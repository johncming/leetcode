package leetcode

func longestPalindrome(s string) string {
	var res string
	if len(s) == 0 {
		return res
	}

	buf := make([][]bool, len(s))
	for i := 0; i < len(s); i++ {
		buf[i] = make([]bool, len(s))
	}

	var max int

	for i := 0; i < len(s); i++ {
		for j := 0; j <= i; j++ {
			if s[i] == s[j] && (i-j <= 2 || buf[i-1][j+1]) {
				buf[i][j] = true
			}
			if buf[i][j] {
				if i-j+1 > max {
					max = i - j + 1
					res = s[j : i+1]
				}
			}
		}
	}

	return res
}
