package leetcode

func isMatch(s string, p string) bool {

	// 初始化dp
	var dp [][]bool
	for i := 0; i < len(s)+1; i++ {
		var tmp []bool
		for j := 0; j < len(p)+1; j++ {
			tmp = append(tmp, false)
		}
		dp = append(dp, tmp)
	}
	// 为了计算方便，dp下标和（s和p）错了一位，例如
	// s[0], p[0]在本解法中对应dp[1][1]
	dp[0][0] = true

	// aab c*aab
	for i := 0; i < len(p); i++ {
		if p[i] == '*' && dp[0][i-1] {
			dp[0][i+1] = true
		}
	}

	for i := 0; i < len(s); i++ {
		for j := 0; j < len(p); j++ {
			//  aa aa
			if p[j] == s[i] {
				dp[i+1][j+1] = dp[i][j]
			}
			// aa a.
			if p[j] == '.' {
				dp[i+1][j+1] = dp[i][j]
			}
			// aa a*
			if p[j] == '*' {
				// aa b*
				if p[j-1] != s[i] && p[j-1] != '.' {
					dp[i+1][j+1] = dp[i+1][j-1]
				} else {
					dp[i+1][j+1] = dp[i+1][j] || dp[i][j+1] || dp[i+1][j-1]
				}
			}
		}
	}

	return dp[len(s)][len(p)]
}
