package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	s := "aa"
	p := "aa"

	res := isMatch(s, p)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	s := "aa"
	p := "cc*aa"

	res := isMatch(s, p)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test3(t *testing.T) {
	s := ""
	p := ".*"

	res := isMatch(s, p)
	fmt.Printf("%+v\n", res) // output for debug
}
