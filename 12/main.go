package leetcode

import (
	"strings"
)

func intToRoman(num int) string {
	values := []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}
	strs := []string{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"}

	var res []string

	for i := 0; i < len(values); i++ {
		// 3000 - 1000 = 2000
		// 2000 -  1000 = 1000
		// 此处需要loop
		for num >= values[i] {
			num -= values[i]
			res = append(res, strs[i])
		}
	}

	return strings.Join(res, "")
}
