package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	res := intToRoman(19)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	res := intToRoman(2)
	fmt.Printf("%+v\n", res) // output for debug
}
