package leetcode

func romanToInt(s string) int {
	if len(s) == 0 {
		return -1
	}

	res := toNum(s[0])
	for i := 1; i < len(s); i++ {
		// IV
		if toNum(s[i]) > toNum(s[i-1]) {
			res += toNum(s[i]) - 2*toNum(s[i-1])
		} else {
			res += toNum(s[i])
		}
	}

	return res
}

func toNum(c byte) int {
	res := 0
	switch c {
	case 'I':
		res = 1
	case 'V':
		res = 5
	case 'X':
		res = 10
	case 'L':
		res = 50
	case 'C':
		res = 100
	case 'D':
		res = 500
	case 'M':
		res = 1000
	}
	return res
}
