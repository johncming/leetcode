package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	s := "DCXXI" // 621
	res := romanToInt(s)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	s := "MCMXCVI" // 1996
	res := romanToInt(s)
	fmt.Printf("%+v\n", res) // output for debug
}
