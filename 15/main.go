package leetcode

import (
	"sort"
)

func threeSum(nums []int) [][]int {
	var res [][]int
	if len(nums) == 0 {
		return res
	}

	sort.Ints(nums)

	for i := 0; i < len(nums)-2; i++ {
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		low, high, sum := i+1, len(nums)-1, 0-nums[i]
		for low < high {
			if nums[low]+nums[high] == sum {
				var tmp []int
				tmp = append(tmp, nums[i], nums[low], nums[high])
				res = append(res, tmp)
				for low < high && nums[low] == nums[low+1] {
					low++
				}
				for low < high && nums[high] == nums[high-1] {
					high--
				}
				low++
				high--
			} else if nums[low]+nums[high] < sum {
				low++
			} else {
				high--
			}
		}
	}

	return res
}
