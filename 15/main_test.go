package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	nums := []int{-1, 0, 1, 2, -1, -4}
	res := threeSum(nums)
	fmt.Printf("%+v\n", res) // output for debug
}
