package leetcode

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	dummy := &ListNode{}
	p1, p2, cur := l1, l2, dummy
	sum := 0

	for p1 != nil || p2 != nil {
		if p1 != nil {
			sum += p1.Val
			p1 = p1.Next
		}
		if p2 != nil {
			sum += p2.Val
			p2 = p2.Next
		}
		cur.Next = &ListNode{Val: sum % 10}
		sum /= 10
		cur = cur.Next
	}
	if sum == 1 {
		cur.Next = &ListNode{Val: 1}
	}

	return dummy.Next
}
