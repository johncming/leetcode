package leetcode

import (
	"strings"
)

func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}

	res := strs[0]

	for i := 1; i < len(strs); i++ {
		for strings.Index(strs[i], res) != 0 {
			res = res[:len(res)-1]
		}
	}

	return res
}
