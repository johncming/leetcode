package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	strs := []string{"a"}
	res := longestCommonPrefix(strs)
	fmt.Printf("%+v\n", res) // output for debug
}
