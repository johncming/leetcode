package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	x := 12321
	res := isPalindrome(x)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	x := 0
	res := isPalindrome(x)
	fmt.Printf("%+v\n", res) // output for debug
}
