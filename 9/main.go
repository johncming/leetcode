package leetcode

func isPalindrome(x int) bool {
	if x < 0 || (x != 0 && x%10 == 0) {
		return false
	}

	y := x
	res := 0

	for x > 0 {
		res = res*10 + x%10
		x /= 10
	}

	return y == res
}
