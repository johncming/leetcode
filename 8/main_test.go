package leetcode

import (
	"fmt"
	"testing"
)

func Test1(t *testing.T) {
	str := "-12v"
	res := myAtoi(str)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test2(t *testing.T) {
	str := "9223372036854775809"
	res := myAtoi(str)
	fmt.Printf("%+v\n", res) // output for debug
}

func Test3(t *testing.T) {
	str := "18446744073709551617"
	res := myAtoi(str)
	fmt.Printf("%+v\n", res) // output for debug
}
