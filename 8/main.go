package leetcode

import (
	"strings"
)

func myAtoi(str string) int {
	str = strings.Trim(str, " ")
	if len(str) == 0 {
		return 0
	}

	sign := 1
	var start int
	switch str[0] {
	case '+':
		sign = 1
		start = 1
	case '-':
		sign = -1
		start = 1
	}

	var res int

	max := 1<<31 - 1

	for i := start; i < len(str); i++ {
		if str[i] < '0' || str[i] > '9' {
			break
		}
		res = res*10 + int(str[i]-'0')

		// 越界判断每次都需要检查，防止res循环中直接越界
		if sign == 1 {
			if res < 0 || res > max {
				res = max
				break
			}
		}

		if sign == -1 {
			if res < 0 || res > max+1 {
				res = max + 1
				break
			}
		}
	}

	return res * sign
}
